package com.charlye934.databinding

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.anushka.viewmodeldemo1.MainActivityViewModel

class MainActivityViewModelFactory(private val startinTotal: Int) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(MainActivityViewModel::class.java)){
            return MainActivityViewModel(startinTotal) as T
        }
        throw IllegalAccessException("Unknow View Model Class")
    }
}