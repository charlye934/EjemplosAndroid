package com.anushka.viewmodeldemo1

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainActivityViewModel(startingTotal: Int) : ViewModel() {
    private var total = MutableLiveData<Int>()
    val totalData: LiveData<Int> = total

    val inputText = MutableLiveData<String>()
    val userName = MutableLiveData<String>()

    init {
        inputText.value = "0"
        total.value = startingTotal
        userName.value = "Carlos"
    }

    fun setTotal(){
        val intInput: Int = inputText.value!!.toInt()
        total.value = (total.value)?.plus(intInput)
    }
}